---
title: "About"
date: 2019-07-03T01:12:55-05:00
draft: false
---

Spencer Krum is a developer and developer advocate working for IBM.

Spencer Krum is a Developer Adovcate at IBM. He writes python (and recently go) applications to analyze esports and deploys them on kubernetes. Before that, he administered the  development infrastructure for OpenStack and wrote a book on Puppet. He lives and works in Minneapolis. He likes cheeseburgers, tennis and StarCraft II.

This website contains source code samples, links to videos and public speaking, and occasional blog posting. 

Connect with Spencer variously around the web.
