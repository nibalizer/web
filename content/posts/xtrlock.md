---
title: "Half lock your screen with xtrlock"
date: 2019-07-10T13:44:57-05:00
draft: false
---

(Originally posted to [dev.to](https://dev.to/nibalizer/xtrlock-1ebf))

Part of the devrel life is the booth. When I'm boothing, I like to have my laptop there and open to a cool project or demo. If I'm lucky, there will be a TV or monitor for me to connect to as well.

Folks will come up and talk to me and ask me questions. That's great and literally what I am there for. But I get squeamish about being a step or two away from my unlocked computer. I also don't want to close or lock my computer. A lock screen looks weird in a booth. If my laptop is closed it is annoying to tell someone 'let me show you!' then we both wait around for an agonizing few seconds while I reconnect to wifi/ssh/whatever.

Enter `xtrlock(1)`. This utility has existed since at least 1993 and is part of a wonderful ecosystem of old Unix/X11 tools that have been largely forgotten. 

From the man page: `Lock X display until password supplied, leaving windows visible`. This locks the screen for input, but leaves whatever I had open visible. That means slides, terminals, or demos are still being displayed and I have peace of mind that I'm not being compromised.

Pratically, `xtrlock` takes effect immediately and has no option for a delay. So what I actually do is type `sleep 5; xtrlock` and use the five second sleep to hide/minimize whatever terminal that command was written in.

I show various things, but one thing I'm trying to do is have terminal based demo scripts that can be run without my interaction. Unlike videos, they are running against real infrastructure, and I can `^C` the script and dig in deeper to the demo with an attendee if I need to. That repo is [here](https://github.com/nibalizer/booth-demos)
