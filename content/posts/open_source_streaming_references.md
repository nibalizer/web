---
title: "References for Open Source in Streaming Talk"
date: 2019-08-20T10:15:29-05:00
draft: false
---


Today I am giving a talk at [Open Source in Gaming (OSIG)](https://events.linuxfoundation.org/events/open-source-in-gaming-day-2019/). These are the references for that talk. I hope to put the core of the talk into a blog post soon.



## Open Source Repositories:

* Twitch Title Updater: <https://github.com/BurnySc2/TwitchTitleUpdater>
* Tensorflow w/ OBS <https://github.com/sergeykalutsky/vsc>
* Library for OBS sources: <https://github.com/nodecg/nodecg>
* SC2 Recent Games Status: <https://github.com/nibalizer/obs-scripts-examples/blob/master/sc2client.py>
* Chat Votes Camera: <https://github.com/nibalizer/obs-scripts-examples/blob/master/chat-votes-camera.py>
* StreamLink: <https://github.com/streamlink/streamlink>
* Twitch Chat Lib: <https://docs.tmijs.org/v1.3.0/Commands.html#part>
* OBS Text source for Shell: <https://github.com/nibalizer/last-command-daemon>
* Twitch API: <https://github.com/tsifrer/python-twitch-client>
* Web Socket Lib: <https://socket.io/>
* Web Socket Control for OBS: <https://github.com/Palakis/obs-websocket>



## Tutorials/How-Tos:

* Using Twitch IRC Chat: <https://gist.github.com/noromanba/df3d975613713f60e6ae>
* Updating Twitch Title for sc2: <https://www.reddit.com/r/starcraft/comments/4zvf9p/guide_sc2_client_api_python_automated_twitch/>




## Misc Specifications:

* SC2 Client API: <https://us.battle.net/forums/en/sc2/topic/20748195420>
* HLS / HTTP Live Streaming RFC: <https://tools.ietf.org/html/rfc8216>
* RTMP Spec: <https://www.adobe.com/devnet/rtmp.html>


## Media and Industry Reports:

* <https://influencermarketinghub.com/25-useful-twitch-statistics/>
* <https://blog.streamlabs.com/youtube-contends-with-twitch-as-streamers-establish-their-audiences-6a53c7b28147>
* <https://newzoo.com/insights/articles/more-people-are-streaming-on-twitch-but-youtube-is-the-platform-of-choice-for-mobile-game-streamers/>
* <https://techcrunch.com/2019/07/12/twitch-continues-to-dominate-live-streaming-with-its-second-biggest-quarter-to-date/>


## Project Websites:

* VLC: <http://www.videolan.org/>
* FFMPEG: <https://ffmpeg.org/>
* OBS Plugins: <https://obsproject.com/forum/resources/categories/obs-studio-plugins.6/>
* OBS Scripts: <https://obsproject.com/forum/resources/categories/scripts.5/>


## What's missing(Probably exists but I haven't seen it):

* Generic 'twitch votes' bot code
* Some kind of 'good enough' open rtmp spec
