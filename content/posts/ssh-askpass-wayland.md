---
title: "Using ssh-askpass with wayland"
date: 2021-09-20T21:22:13-05:00
draft: false
---

Before moving to wayland, I always used an ssh-agent with `ssh-add -c`. This prompts me every time it wants to use my agent. This is a nice middle ground between security and convenience. However, since moving to wayland, I've been unable to get that working. Here are the hacks/learnings I have gained since sitting down to do this. [This](https://www.pbdigital.org/post/2020-01-06-ssh-keys-with-confirmation-fedora/) is a great writeup but it doesn't cover getting it working in wayland.

`SSH_ASKPASS` and `DISPLAY` need to be set on the `ssh-agent` process, not on your ssh client process. I start my agent at the same time I launch [sway](https://swaywm.org/).

Yes, display needs to be set even in wayland.

```
#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
   # Useful env variables
   export MOZ_ENABLE_WAYLAND=1
   eval $(DISPLAY=:0 ssh-agent)
   # for debugging ssh agent
   #ssh-agent -d 2>/tmp/ssh-agent.log &

   XKB_DEFAULT_LAYOUT=us exec sway
fi
```


`SSH_ASKPASS` defaults (on Arch) to `/usr/lib/ssh/ssh-askpass` if you'd prefer to place your tool there.

A zenity based script I'm using:

```

#!/bin/bash

# title is important here: sway config scans title to set floating
/usr/bin/zenity --title ssh-askpass --question --text "A PROGRAM REQUIRES AN AGENT. \n Approve?"

```


Then you need to float the dialog so it's not another tiled window. Based on [this issue](https://github.com/swaywm/sway/issues/3095) it seems like the only way to do this is hacks. So here is my sway config snippet that makes the window float. Note that this floats anything with `ssh-askpass` in the title so it could certainly be improved.


```
for_window {
  [title="ssh-askpass"] {
    floating enable
  }
}
```
