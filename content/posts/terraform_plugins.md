---
title: "Terraform Plugins"
date: 2020-11-18T19:59:11Z
draft: false
---

Terraform now has an app store of sorts. The [terraform registry](https://registry.terraform.io/browse/providers) is a web host for plugins and providers, and has tooling integration.

In attempting to use the [IBM Cloud](https://registry.terraform.io/providers/IBM-Cloud/ibm/latest) provider, I ran into the following errors:

```bash
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/ibm...

Error: Failed to install provider

Error while installing hashicorp/ibm: provider registry registry.terraform.io
does not have a provider named registry.terraform.io/hashicorp/ibm
```

The tool seems to want to install the provider from the `hashicorp` namespace and not the `ibm-cloud` namespace.


I fixed this issue by creating a `versions.tf` file.

```bash
$ cat versions.tf
```

```hcl
terraform {
  required_providers {
    ibm = {
      source  = "IBM-Cloud/ibm"
    }
  }
}
```

And then `terraform init` can proceed normally:

```
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of ibm-cloud/ibm...
- Installing ibm-cloud/ibm v1.14.0...
- Installed ibm-cloud/ibm v1.14.0 (self-signed, key ID AAD3B791C49CC253)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/plugins/signing.html

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, we recommend adding version constraints in a required_providers block
in your configuration, with the constraint strings suggested below.

* ibm-cloud/ibm: version = "~> 1.14.0"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```



For completeness, my environment is below. The official IBM Terraform docs talk you through setting up provider.tf, environment variables, etc

```
$ terraform version
Terraform v0.13.4

```

```bash
$ cat provider.tf
```

```hcl
variable "ibmcloud_api_key" {}
variable "iaas_classic_username" {}
variable "iaas_classic_api_key" {}

provider  "ibm" {
  ibmcloud_api_key   = var.ibmcloud_api_key
  generation         = 2
  region             = "us-south"
  iaas_classic_username = var.iaas_classic_username
  iaas_classic_api_key  = var.iaas_classic_api_key
}
```



