---
title: "Upcoming Projects"
date: 2019-11-09T20:36:08-06:00
draft: false
---

It's always risky to announce projects that aren't complete yet. But I'm very excited about a few projects on the docket to work on next. I have a collection of weird gear in the pile that I can't wait to restore, hack up, and repurpose. I don't have a start or completion date for any of these projects and the exact details are still waiting to get figured out. The order in this list is random, the project I'm the most excited about is the PiDP-11.

## iMac G3

I have an iMac G3 and flatscreen monitor of approximately the correct size and aspect ratio (a surprisingly difficult part to source). The monitor doesn't have a power supply. The iMac does work. I'll probably gut the machine and try to put modern parts inside. The very small screen size and aspect ratio makes it hard to imagine using it for anything.

![netra](/posts/img/upcoming-projects/imac-g3.jpeg)
![netra](/posts/img/upcoming-projects/imac-g3-2.jpeg)

## iMac G4 

I have an iMac G4 that I can hack up. I might just put Linux on this and use it as an information radiator box. It is possible to rewire the screen to a normal DVI output and replace the guts with modern hardware, but I'm not sure that'd be worth it. On older Linux (Ubuntu Hardy Heron) graphics worked fine, on modern versions of ubuntu (to the extent that PPC is even supported) there are terrible graphical errors. So getting this to work well might be a challenge.


![netra](/posts/img/upcoming-projects/imac-g4.jpeg)


## Light Board

I have a control board for a theater lighting system. I picked it up because it has a crap-ton of analog switches on it. I can imagine building a mondo analog controller for anything powered by a microcontroller. This might be a solution looking for a problem. It also sounds super fun.

![netra](/posts/img/upcoming-projects/light-board.jpeg)
![netra](/posts/img/upcoming-projects/light-board2.jpeg)

## Sun Netra

I have an ancient half depth 1U sun server (sparc64!) I could build some custom linux to run on it. Or explore putting modern server components in this sick case from sun.

![netra](/posts/img/upcoming-projects/netra.jpeg)
![netra](/posts/img/upcoming-projects/netra2.jpeg)


## PiDP-11

I have a kit to build a PDP-11 front panel for a raspberry pi. You can see the link [here](https://obsolescence.wixsite.com/obsolescence/pidp-11). This project is going to be super fun, I'm almost saving it for the right moment so I can relish in the project.


