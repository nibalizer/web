---
title: "Cloud Init Trivia"
date: 2020-12-02T22:46:23Z
draft: false
---

Cloud init is the ubiquitous first-run config software used by most cloud providers. Recently I was messing with it trying to get some configuration to work, it's always been a tool that requires a lot of fiddling. I wanted to write down my tips for debugging cloud init.


# First and foremost

It says this on the website but the most common reasons the cloud-init user data isn't working are these two reasons:

1. YAML formatting problem
2. `#cloud-config` missing from the top of the file

# Disabling 

You can disable cloud init by passing `cloud-init=disabled` on the kernel command line.


# After ssh in, locally reproduce

It's a lot faster to debug and iterate with a running system than to re-launch a vm.

## Logs

`/var/log/cloud-init.log`
`/var/log/cloud-init-output.log`


## Reset (hard)

Reset all the state and re-run cloud init (warning this will regen ssh keys)

```
cloud-init clean
cloud-init init
```

More manual and leaves bits around to inspect
```
rm /var/lib/cloud/instances/1713d6c9-0969-4e69-935c-85c5c29a7bd2/sem
```

Your actual user data
```
cat /var/lib/cloud/instance/user-data.txt
```

Re run a single module from cloud-init/cloud-config

```
cloud-init single --name foobar
```

## Debug

```
export DEBUG=2
```


## Inspect cloud-init from a different script

The docs say you can poll on `cloud-init status` if you want to wait for cloud-init to finish

```
# cloud-init status --long
status: running
time: Wed, 02 Dec 2020 22:53:49 +0000
detail:
DataSourceNoCloudNet [seed=/dev/vdb][dsmode=net]
```

## Inspect ConfigDrive/NoCloud manually

```
# mkdir /media/b
# mount /dev/vdb /media/b
mount: /media/b: WARNING: device write-protected, mounted read-only.
# ls /media/b/
meta-data  user-data  vendor-data
```

