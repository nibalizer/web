---
title: "SSH HoneyPot"
date: 2015-01-18T19:56:59-07:00
draft: false
---

Today we will build an ssh honeypot. An ssh honeypot is a program that pretends to be a an ssh daemon, but when an attacker connects, it lets them in. It provides a safe but realistic fake environment to the attacker, and logs everything they do. This means we can see what the attackers do after they try to gain access to our system.

This will be part one of a two part post. In this first post we will set up the service, in the second post we will analyze the logs that were generated.


Basic stats:

* Ram: 512MB
* Ubuntu 14.04 Trusty
* ipv6: yes


My first step is to move ssh off of port 22 and onto port 2121. This second port was chosen at random. My next step is to enable ssh access by key only. It is safe to restart the sshd daemon while ssh'd into the machine, there is magic with file descriptors that allows your ssh connection to stay alive as the daemon is being restarted.


Here is my /etc/ssh/sshd_config


```
# Package generated configuration file
# See the sshd_config(5) manpage for details

# What ports, IPs and protocols we listen for
Port 2121
# Use these options to restrict which interfaces/protocols sshd will bind to
#ListenAddress ::
#ListenAddress 0.0.0.0
Protocol 2
# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
#Privilege Separation is turned on for security
UsePrivilegeSeparation yes

# Lifetime and size of ephemeral version 1 server key
KeyRegenerationInterval 3600
ServerKeyBits 1024

# Logging
SyslogFacility AUTH
LogLevel INFO

# Authentication:
LoginGraceTime 120
PermitRootLogin without-password
StrictModes yes

RSAAuthentication yes
PubkeyAuthentication yes
#AuthorizedKeysFile%h/.ssh/authorized_keys

# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes
# For this to work you will also need host keys in /etc/ssh_known_hosts
RhostsRSAAuthentication no
# similar for protocol version 2
HostbasedAuthentication no
# Uncomment if you don't trust ~/.ssh/known_hosts for RhostsRSAAuthentication
#IgnoreUserKnownHosts yes

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Change to no to disable tunnelled clear text passwords
PasswordAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosGetAFSToken no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes

X11Forwarding no
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
#UseLogin no

#MaxStartups 10:30:60
#Banner /etc/issue.net

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

Subsystem sftp /usr/lib/openssh/sftp-server

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM no
```

The honeypot software we are going to run is <a href='https://github.com/desaster/kippo'>kippo</a>. Kippo is a python program and we will be running it in a python virtualenv. We will create a kippo user to run the daemon. We will not run kippo as root, and we will use iptables to map port 22 to port 2222.

Adding a user:

```
# adduser kippo
# press return a bunch, don't set a password
```

Getitng the source code:

```
git clone https://github.com/desaster/kippo.git
Cloning into 'kippo'...
remote: Counting objects: 1532, done.
remote: Total 1532 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1532/1532), 2.63 MiB | 4.46 MiB/s, done.
Resolving deltas: 100% (933/933), done.
Checking connectivity... done.
```

Creating and sourcing the virtualenv:

```
kippo@honeypot:~/kippo$ virtualenv venv
New python executable in venv/bin/python
Installing setuptools, pip...done.
kippo@honeypot:~/kippo$ source venv/bin/activate
(venv)kippo@honeypot:~$
```


We can start with the distributed kippo config file. We make only small changes to it.
Create config file:


```
kippo@honeypot:~/kippo$ diff kippo.cfg kippo.cfg.dist 
21c21
< hostname = compute21
---
> hostname = svr03
139c139
< ssh_version_string = SSH-2.0-OpenSSH_5.5p1 Debian-6+squeeze1
---
> ssh_version_string = SSH-2.0-OpenSSH_5.1p1 Debian-5
```


Now start the daemon:

```
kippo@honeypot:~/kippo$ ./start.sh 
twistd (the Twisted daemon) 13.2.0
Copyright (c) 2001-2013 Twisted Matrix Laboratories.
See LICENSE for details.
Starting kippo in the background...
Generating new RSA keypair...
Done.
Generating new DSA keypair...
Done.
```

And now we can connect up to it:


```
$: ssh -l root -p 2222 honeypot.nibalizer.com
Password:
root@compute21:~# uptime
01:07:20 up 6 min,  1 user,  load average: 0.00, 0.00, 0.00
root@compute21:~# init 6
bash: init: command not found
root@compute21:~# reboot

Broadcast message from root@compute21 (pts/0) (Mon Jan 19 01:07:27 2015):

The system is going down for reboot NOW!



Connection to server closed.
root@localhost:~# 
```


Of course the server didn't actually reboot. And we can verify that logs are being created in ~kippo/kippo/log

```
kippo@honeypot:~/kippo$ ls log/tty/
20150119-010112-8620.log  20150119-010717-9734.log
```

Kippo has the neat feature that it saves any files downloaded. Be EXTRA careful when interacting with these:

```
kippo@honeypot:~/kippo$ ls dl
20150119010917_http___google_com
```

The only thing remaining to do is wire up port 22 to port 2222. We can do that with a bit of iptables:


```
iptables -t nat -A PREROUTING -p tcp --dport 22 -j REDIRECT --to-port 2222
```

And there we have it! A honeypot server is up and running and ready to accept brute force attacks. This honeypot server is set to accept root logins with the password 123456. You can hit it yourself if you like: honeypot.nibalizer.com. Next week we will analyze our results, possibly even generating graphs with kibana.

