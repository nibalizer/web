---
title: "Creating an OBS source for serial devices"
date: 2019-11-08T14:04:59-06:00
draft: false
---

As part of my recent live streaming on twitch I've been working with a microcontroller connected over USB. Actual communication with the microcontroller is done over serial. The device presents itself as `/dev/ttyACM0`. 

The developer loop is basically this:

* Wire up circuit
* Write up some code
* Change code
* Flash microcontroller
* Debug:
 * Watch for LED blinks
 * Read logs over serial
* GOTO: 1


It's straightforward to read the serial device via `screen`:

```
sudo chown :nibz /dev/ttyACM0
# or add self to group dialout
# or use sudo on screen
screen /dev/ttyACM0
```

All good and this works fine. However, I can do better. When I'm working live via twitch, I want to give the viewers as much info about the system I'm working with as possible. Not only does this give them the best chance to help me debug, but it also makes my stream unique. So, how do I get the output of my serial communications dumped onto the OBS screen?


The most obvious way to do this is to capture a terminal running screen. This is less than ideal because it would take up a lot of screen real estate on my cast.

Another idea is to write a small daemon that reads the output from the serial device and exposes that via a web/javascript frontend that I can pick up as a Browser Source in OBS. That would probably work, but would be a lot of programming.

Instead I did this:

* Make a new profile for Konsole (my terminal editor)
* Set font to huge, bold, and white
* Set background to electric green
* Run screen on the serial device
* Window capture terminal into OBS
* Apply filter - Chroma key

This brings in terminal text with a transparent background so it can be layered over a secondary camera or other less important piece of screen real estate. The only downside of the current approach is that the `screen` utility has total control over `/dev/ttyACM0` so I must kill screen to flash the microcontroller (and remember to restart it).

This is a dirty hack and I love it and I feel bad for doing it. Screenshots below.



![Basic Terminal](/posts/img/serial-obs/green_terminal.png)
![Detail on terminal settings](/posts/img/serial-obs/terminal_settings.png)
![Terminal captured in OBS](/posts/img/serial-obs/obs_terminal_no_chroma.png)
![Chroma Key settings](/posts/img/serial-obs/chroma_key_settings.png)
![Final Product - text from terminal with transparent background as OBS source](/posts/img/serial-obs/final_product.png)
