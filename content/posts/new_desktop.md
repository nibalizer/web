---
title: "New Desktop"
date: 2019-10-01T12:04:46-05:00
draft: false
---

# Introduction

I haven't taken notes on a soup to nuts Linux reinstall in several years so I thought I'd put my notes on reinstallation here for myself and for posterity. It is interesting to me that despite being in a mainstream and quick moving Linux distribution, I had to do an incredible amount of configuration outside of the package manager.

![archey screenshot](/posts/img/cropped_archey.png)

There are a couple pieces here that make this easier. For one I've been keeping 'the big list of packages' for about a year now checked in to git. For another I finally have a dotfiles management system that I'm happy with. I've had dotfiles in git for a long time but setting up a new system has always been a lot of editor copy/paste work.

Those repos are here [packages list](https://github.com/nibalizer/personal-ansible/tree/master/scripts) and [dotfiles repo](https://github.com/nibalizer/dotfiles).


# Ubuntu Installer

* Install stock Ubuntu 18.04 from USB stick
* select extra packages during installation
* select colemak keyboard layout during installation
* select full disk encryption during installation

Installer re-used EFI partition created by windows installation. Set Ubuntu ahead of windows in the boot order (how?).


# First boot steps

## Update Ubuntu

```shell
root@huygens:~# apt update
root@huygens:~# apt upgrade -y
root@huygens:~# apt dist-upgrade -y
```

# Firefox

## Install Firefox beta channel


```shell
nibz@huygens:~$ mkdir -p ~/local/bin
nibz@huygens:~$ cp ~/Downloads/firefox-70.0b6.tar.bz2 local/
nibz@huygens:~$ cd ~/local/
nibz@huygens:~/local$ ls
bin
nibz@huygens:~/local$ mv ~/Downloads/firefox-70.0b6.tar.bz2 .
nibz@huygens:~/local$ ls
bin  firefox-70.0b6.tar.bz2
nibz@huygens:~/local$ tar -xvjf firefox-70.0b6.tar.bz2 
firefox/firefox
nibz@huygens:~/local$ cd bin/
nibz@huygens:~/local/bin$ ls
nibz@huygens:~/local/bin$ ln -s ../firefox/firefox
nibz@huygens:~/local/bin$ ls -l
total 0
lrwxrwxrwx 1 nibz nibz 18 Sep 13 20:55 firefox -> ../firefox/firefox
```

##  Create firefox Profiles

```shell
cd local/bin
./firefox --new-instance --ProfileManager
```

Make 3 new profiles:

* Work
* Main
* misc/bomb-disposal


## Profile configuraton


* Set search to duckduckgo
* uncheck: Show search suggestions ahead of browsing history in address bar results

* Install Tree Style Tabs

  * https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/

* Install ublock origin

  * https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/

* Set about:config px per px (layout.css.devPixelsPerPx) to make  firefox work normally on highdpi screens. Set to 1.5.

* Install Window Titler: 
  * https://addons.mozilla.org/en-US/firefox/addon/window-titler/

* Click titler toolbar - click gear - set profile wide title for profile

* (optional, install theme)


# Install core utilities

```shell
root@huygens:~# apt install vim git apt-transport-https
```


# Install personal package list

```shell
root@huygens:~# git clone https://github.com/nibalizer/personal-ansible
root@huygens:~# ln -s personal-ansible/scripts/* .
root@huygens:~# ls
packages.yaml  install_packages.sh
root@huygens:~# vim packages.yaml  # edit out any packages that depend on a not-yet-added repo/ppa
root@huygens:~# ./install_packages.sh 
root@huygens:~# vim packages.yaml  # edit out any packages that don't exist in ubuntu
root@huygens:~# ./install_packages.sh
```

# Setup SSH

## SSH client

```shell
# Generate ssh key
nibz@huygens:~$ ssh-keygen -t rsa -b 4096
# Pull keys this host should trust (possibly editing after)
nibz@huygens:~$ curl https://github.com/nibalizer.keys >> .ssh/authorized_keys
```

(copy over ~/.ssh/config and ~/.ssh/known_hosts if needed)

Or client configuration:

```
ForwardAgent no
UseRoaming no
#VisualHostKey yes
VerifyHostKeyDNS ask
StrictHostKeyChecking ask
Compression yes
#ConnectTimeout 5
ServerAliveInterval 30
#openssl speed aes rc4 blowfish
#Ciphers blowfish-cbc,aes256-cbc
ForwardX11 no
ForwardX11Trusted no
#SendEnv TERM
NoHostAuthenticationForLocalhost yes
HashKnownHosts no
```

## SSH Server (sshd)

```shell
root@huygens:~# apt install openssh-server
```

Disable password auth:

Change: `#PasswordAuthentication yes` to `PasswordAuthentication no`

```shell 
root@huygens:~# vim /etc/ssh/sshd_config 
root@huygens:~# systemctl restart ssh 
#setup /etc/hosts if needed
root@huygens:~# sudo vim /etc/hosts
```

# switch to KDE

```shell
root@huygens:~# apt install tasksel

root@huygens:~# tasksel install kubuntu-desktop
# select gdm when prompted
#(reboot)
```

# KDE customizations

* Keyboard will be in Colemak
* Set Caps Lock to control
* System Settings -> Keyboard -> Advanced > Ctrl Position -> Checkbox "Caps lock as control"
* Increase bottom panel size: Right click -> system tray settings, re-size
* Set face pic. KDE menu -> Click name -> Click icon -> Load from file (requires auth lol)


# Install proprietary Nividia drivers

(from nixcraft) 

```shell
root@huygens:~# hwinfo --gfxcard --short
graphics card:                                                  
                       nVidia GP104 [GeForce GTX 1070]

Primary display adapter: #14
```

```shell
root@huygens:~# lshw -C display
  *-display                 
       description: VGA compatible controller
       product: GP104 [GeForce GTX 1070]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:01:00.0
       version: a1
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress vga_controller bus_master cap_list rom
       configuration: driver=nouveau latency=0
       resources: irq:124 memory:f6000000-f6ffffff memory:e0000000-efffffff memory:f0000000-f1ffffff ioport:e000(size=128) memory:c0000-dffff


root@huygens:~# apt install nvidia-driver-430
#(reboot)
```


post-reboot:

```shell
root@huygens:~# lshw -C display
  *-display                 
       description: VGA compatible controller
       product: GP104 [GeForce GTX 1070]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:01:00.0
       version: a1
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress vga_controller bus_master cap_list rom
       configuration: driver=nvidia latency=0
       resources: irq:124 memory:f6000000-f6ffffff memory:e0000000-efffffff memory:f0000000-f1ffffff ioport:e000(size=128) memory:c0000-dffff
```

Note the `driver=nvidia`

#  Install steam

```shell
root@huygens:~# apt install steam # 2019 living in the future #hype
```

```shell
nibz@huygens:~$ steam
```

* Log in, 2fa etc

* Set up remote download folder

  * View -> Settings -> Steam Library Folders -> Add Library Folder
  * set this to whatever remote storage you have available.
  * right click - make default
  * When installing a game, set this folder if the default failed


# Spotify

```shell
nibz@huygens:~$ sudo snap install spotify
```

Launch & Login

# Install Lutris & Wine

## Lutris

```shell
# lutris extra libs
https://github.com/lutris/lutris/wiki/Installing-drivers

root@huygens:~# add-apt-repository ppa:graphics-drivers/ppa
root@huygens:~# dpkg --add-architecture i386 

#install libs (listed in packages.yaml)

root@huygens:~# add-apt-repository ppa:lutris-team/lutris
root@huygens:~# apt install lutris
#(reboot)
```

## Wine

```shell
root@huygens:~# wget -nc https://dl.winehq.org/wine-builds/winehq.key
root@huygens:~# apt-key add winehq.key
root@huygens:~# apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
```

* Wine first launch will perform many installs/setups
* Set lutris defult directory (remote storage?)
* Set install directory per game
* Install games from internet[2019 whatttt]


# Discord

Download .deb from their website (NEAT)
add a couple packages ( libc++1libappindicator1)
dpkg -i discord.deb

Note: Discord sound is working, I had a pulseaudio issue

# Zoom

* Download .deb for ubuntu 14.04+
* dpkg -i and it works


# Development

## SaaS

* Log in to gitlab
  * Add ssh key to gitlab (audit keys)
* Log in to github
  * Add ssh key to github (audit keys)

TODO: setup token for `hub` utility

## Dotfiles


* Dotfiles stored [in a git repo](https://github.com/nibalizer/dotfiles)
* [YADM](https://yadm.io/) is used to manage dotfiles (basically just git)



```shell
nibz@huygens:~$ yadm clone git@github.com:nibalizer/dotfiles.git
# (this is going to be bloody)
nibz@huygens:~$ yadm reset --hard origin/master
nibz@huygens:~$ yadm submodule init
nibz@huygens:~$ yadm submodule update
```

Spot check and cleanup:

* git username information in `.gitconfig`
* mess with `.vimrc` because of course

## Extra utils

```shell
nibz@huygens:~$ mkdir -p ~/devel/nibalizer
nibz@huygens:~$ cd devel/nibalizer/
nibz@huygens:~$ git clone git@github.com:nibalizer/very_misc_scripts.git 
nibz@huygens:~$ cd very_misc_scripts
# install ibmcloud cli
nibz@huygens:~$ ./install_or_upgrade_ibmcloud_cli.sh 
nibz@huygens:~$ which ibmcloud
/home/nibz/local/bin/ibmcloud
nibz@huygens:~$ ibmcloud login --sso
# install okd/kubectl)
nibz@huygens:~$ ./install_or_upgrade_okd.sh
nibz@huygens:~/devel/nibalizer/very_misc_scripts$ which oc
/home/nibz/local/bin/oc
nibz@huygens:~/devel/nibalizer/very_misc_scripts$ which kubectl 
/home/nibz/local/bin/kubectl
```

## Golang

(from https://github.com/golang/go/wiki/Ubuntu)


```shell
root@huygens:~# add-apt-repository ppa:longsleep/golang-backports
root@huygens:~# apt-get update
./install-packages.sh
```


```shell
nibz@huygens:~/$ go version
go version go1.13.1 linux/amd64
```

Set `export GOPRIVATE=*.ibm.com,${GOPRIVATE}`

* https://golang.org/cmd/go/#hdr-Module_downloading_and_verification
* https://proxy.golang.org/privacy
* https://gist.github.com/nibalizer/f8ecf95966faf1d5a47444dedffc1fab


Install hugo to demonstrate go is working


```shell
nibz@huygens:~/$ go get github.com/gohugoio/hugo
# wait a little
nibz@huygens:~$ which hugo
/home/nibz/go/bin/hugo
nibz@huygens:~$ hugo version
Hugo Static Site Generator v0.59.0-DEV linux/amd64 BuildDate: unknown
nibz@huygens:~$ du -sh ~/go
1.1G    /home/nibz/go
```

## OBS

I use Open Broadcaster Software quite a bit.


Remove obs if it was already isntalled

```shell
apt-remove obs-studo
```


Get OBS from PPA

```shell
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt-get update
sudo apt-get install obs-studio
```

## Get OBS Linux Browser

```shell
wget 'https://github.com/bazukas/obs-linuxbrowser/releases/download/0.6.1/linuxbrowser0.6.1-obs23.0.2-64bit.tgz'
mkdir -p $HOME/.config/obs-studio/plugins
tar -zxvf linuxbrowser0.6.1-obs23.0.2-64bit.tgz -C $HOME/.config/obs-studio/plugins/
```
