---
title: "Delta wifi not working on Linux? Try stopping docker"
date: 2024-05-01T15:05:42-05:00
draft: false
---


Delta's current (2024) wifi uses the `172.19.0.0/23` section of the RFC 1918 space. This conflicts with default Linux+Docker which uses nearby space. To get networking working on Linux while using Delta wifi, simply turn off docker and remove any bridges it created.


On my current laptop I see one `docker0` bridge and several other bridges, presumably created by docker for networking:


```
br-05708c866d2e: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.19.0.1  netmask 255.255.0.0  broadcast 172.19.255.255
        ether 02:42:40:63:86:1e  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

br-16ecd2c40294: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.23.0.1  netmask 255.255.0.0  broadcast 172.23.255.255
        ether 02:42:aa:75:2b:b9  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

br-c547760905b4: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.18.0.1  netmask 255.255.0.0  broadcast 172.18.255.255
        ether 02:42:b5:1a:d0:84  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

docker0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255
        inet6 fe80::42:efff:fe13:914d  prefixlen 64  scopeid 0x20<link>
        ether 02:42:ef:13:91:4d  txqueuelen 0  (Ethernet)
        RX packets 51  bytes 2421 (2.3 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 324  bytes 35264 (34.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

With no action from me, I can connect to the `Deltawifi.com` network but can't hit the captive portal.

To fix this I run the following commands:


```
cat /etc/resolv.conf
ifconfig
brctl show
sudo systemctl stop docker
sudo ifconfig docker0 down
sudo ifconfig br-05708c866d2e down
sudo ifconfig br-16ecd2c40294 down
sudo ifconfig br-c547760905b4 down
sudo brctl delbr docker0
sudo brctl delbr br-c547760905b4
sudo brctl delbr br-16ecd2c40294
sudo brctl delbr br-05708c866d2e

```

I hope this works for you!
