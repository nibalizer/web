---
title: "Django OTP from Django Shell"
date: 2025-01-21T12:05:42-05:00
draft: false
---


When securing Django aps with [django_otp](https://django-otp-official.readthedocs.io/en/stable/) it can be confusing how to manage these from the django shell.

Some quick notes I could have used recently:


```
# Enter django shell
python manage.py shell

```



```
# Imports
import django_otp
```

```
# Get User
user = User.objects.filter(id=100)[0]
```


```
# Get Device
device = django_otp.devices_for_user(user)
```

This is an instance of `django_otp.models.Device`. Or possibly a queryset of several devices if the user has more than one device. I only tested with `TOTPDevice`.

There are a bunch of useful methods on this object:


```
# name
device.name
device.last_used_at

# throttling information
device.throttling_failure_count
device.throttling_failure_timestamp

# throttling reset

device.throttle_reset()

# config url

device.config_url
```

Hopefully if you're trying to fix something this helps you, I had to dig a bunch to find it.
