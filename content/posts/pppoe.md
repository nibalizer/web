---
title: "Using Centurylink gigabit fiber with Debian Linux"
date: 2021-01-19T15:31:36-06:00
draft: false
---

# Introduction

In my city I am able to get gigabit fiber from centurylink. It's not the best service in the world but it's decently priced and gigabit service is incredible compared to Comcast's 100mbit with transfer limits.

CTL provides either paid, rented, or promotional routers to use with their service. I was set up with a C4000XG. In this post I'll describe how to replace the C4000XG with a standard linux router. I am writing this because it wasn't obvious to me how to do it and most of the information online doesn't dive into the linux configuration required.

# Background

CTL fiber to the home or fiber to an apartment is converted into copper by a device called an Optical Network Terminal or ONT. Mine is literally and figuratively a black box that has fiber optic in and ethernet jack and phone jack out[^1]. In an apartment building the ONT might not be exposed to you and CTL will just light up a jack in your apartment for you. In both cases an ethernet cable runs from the ONT into the WAN port on the CTL router. In most cases you can't just plug in any wireless router into the ONT, there is authentication configuration you have to do first. This post is about configuring linux to do the authentication to the ONT.

You need to call CTL and get three pieces of information:

* Your PPPoE username (often a @centurylink.net or @quest.net email address)
* Your PPPoE Password
* The vlan number that your ONT and router are using. (I don't have any great advice on how to describe this to the support person, you may need to ask to be sent higher up to be sure they're reading you the right number. The most common vlan is 201)

# Connection in general

1. Correct vlan ID (communication between the router and the ONT happens on a vlan, not untagged ethernet)
2. Authenticate and establish a PPPoE connection (PPP is point-to-point, PPPoE is PPP over Ethernet) 

# Connection specifics

System: Debian Buster

Packages:

`apt-get install pppoeconf`


Configuration files:

* `/etc/network/interfaces`
* `/etc/ppp/peers/dsl-provider`
* `/etc/ppp/pap-secrets`


## Configure `/etc/network/interfaces`:

```
auto enp4s0.0201
iface enp4s0.0201 inet manual

auto dsl-provider
iface dsl-provider inet ppp
pre-up    /sbin/ifconfig enp4s0.0201 up
provider dsl-provider
```

This is configuring my interface `enp4s0` to come up, but not do dhcp or anything. This also sets up an interface on the `201` vlan, which is the vlan used by my ONT.

The second stanza creates a fake interface `dsl-provider` that will be configured via `ppp` according to the other config files. Note: The name `dsl-provider` is arbitrary, you can change it to whatever you want, so long as you change it everywhere (including the filename in `/etc/ppp/peers`). 


## Configure `/etc/ppp/peers/dsl-provider`

This is snipped. Change the `user` line and the `pty` line (put in your tagged interface).

```
# Stuff to configure...

# MUST CHANGE: Uncomment the following line, replacing the user@provider.net
# by the DSL user name given to your by your DSL provider.
# (There should be a matching entry in /etc/ppp/pap-secrets with the password.)
user <myusername>@centurylink.net

# Use the pppoe program to send the ppp packets over the Ethernet link
# This line should work fine if this computer is the only one accessing
# the Internet through this DSL connection. This is the right line to use
# for most people.

# note this uses the vlan interface
pty "/usr/sbin/pppoe -I enp4s0.0201 -T 80 -m 1452"
```


## Configure `/etc/ppp/pap-secrets`

This is snipped. The quotes are to be included.

```

# Here you should add your userid password to connect to your providers via
# PAP. The * means that the password is to be used for ANY host you connect
# to. Thus you do not have to worry about the foreign machine name. Just
# replace password with your password.
# If you have different providers with different passwords then you better
# remove the following line.

"joebob225@centurylink.net"	*	ABCd12345	
```

## Test

```
# ifup dsl-provider
```

You can `ifdown` to turn it off/cycle it, whatever.


## Verify

When everything succeeded, this is what logs looked like in `/var/log/messages`:

```
Jan 19 00:42:38 mendeleev pppd[1649]: pppd 2.4.7 started by root, uid 0
Jan 19 00:42:38 mendeleev pppd[1649]: Serial connection established.
Jan 19 00:42:38 mendeleev pppd[1649]: Using interface ppp0
Jan 19 00:42:38 mendeleev pppd[1649]: Connect: ppp0 <--> /dev/pts/4
Jan 19 00:42:39 mendeleev pppd[1649]: PAP authentication succeeded
Jan 19 00:42:39 mendeleev kernel: [ 2055.415146] PPP BSD Compression module registered
Jan 19 00:42:39 mendeleev kernel: [ 2055.420928] PPP Deflate Compression module registered
Jan 19 00:42:39 mendeleev pppd[1649]: local  IP address 97.116.XXX.XXX
Jan 19 00:42:39 mendeleev pppd[1649]: remote IP address 207.109.XXX.XXX
Jan 19 00:42:39 mendeleev pppd[1649]: primary   DNS address 205.171.3.25
Jan 19 00:42:39 mendeleev pppd[1649]: secondary DNS address 205.171.2.25
```


```
# ip link show

8: enp4s0.0201@enp4s0: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 00:e0:4c:68:86:d1 brd ff:ff:ff:ff:ff:ff
23: ppp0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1492 qdisc pfifo_fast state UNKNOWN mode DEFAULT group default qlen 3
    link/ppp
```


```
# ps -ef | grep pp[p]

root      1649     1  0 00:42 ?        00:00:00 /usr/sbin/pppd call dsl-provider
root      1651  1649  0 00:42 ?        00:00:00 sh -c /usr/sbin/pppoe -I enp4s0.0201 -T 80 -m 1452
nobody    1654  1651  0 00:42 ?        00:00:00 /usr/sbin/pppoe -I enp4s0.0201 -T 80 -m 1452
```

And of course `ping google.com` and `ip route show`. If you do not have a default route set up when you make the connection, it will set one up for you.

## Common Errors

This error log is from when I had the credentials mis-configured. Real solid debugging output.

```
Jan 19 00:40:28 mendeleev pppd[1373]: pppd 2.4.7 started by root, uid 0
Jan 19 00:40:28 mendeleev pppd[1373]: Serial connection established.
Jan 19 00:40:28 mendeleev pppd[1373]: Using interface ppp0
Jan 19 00:40:28 mendeleev pppd[1373]: Connect: ppp0 <--> /dev/pts/1
Jan 19 00:40:30 mendeleev pppd[1373]: Modem hangup
Jan 19 00:40:30 mendeleev pppd[1373]: Connection terminated.
```


References:

* https://wiki.debian.org/PPPoE
* https://bidetly.io/2020/03/20/centurylink-fiber-on-pfsense/
* https://steele.blue/c4000xg-ppoe-password/
* https://old.reddit.com/r/centurylink/comments/ic4asm/howto_you_may_not_need_that_c4000xg_or_whatever/
* https://manpages.debian.org/buster/vlan/vlan-interfaces.5.en.html



[^1]: When the tech came out to set it up, he performed the final configuration by jacking into the phone port with his telephone and typing in some codes and having a conversation with a robotic voice.
