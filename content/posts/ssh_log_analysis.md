---
title: "SSH Log Analysis"
date: 2015-01-11T20:47:28-07:00
draft: false
---


Today we will analyze ssh logs on a server on the open internet. These connections generally fall into two categories, successful logins from legitimate users, and failed logins from attempted ssh brute force attacks. We will provide no information on the successful logins and will provide high level analysis of the unsuccesful logins.


Basic stats:

* First log event: Jan 11 03:53:39<
* Last log event: Jan 12 05:41:32
* Total log lines: 44853
* OS Version: Centos 6.5
* sshd: Version     : 5.3p1 (from yum)
* sshd: Release     : 104.el6_6.1 (from yum)


Light analysis:

* Failed password authentication for root: 22199
* Failed password for other users: 209
* Other users:

```
admin
appcadmin
arun
binbin
dasusr1
db2fenc1
db2fenc2
edx
elbarassin
evanss
galbatorix
ghost
gusrli
hadoop
ham
icang
info
kang
kenny
kjclub
klindley
mrlong
solr
tansya
tester
ubnt
vagrant
```


Some of these users are quite interesting. 'admin' is of course a classic, as are 'info' and 'tester'. But 'hadoop', 'vagrant', and 'solr'  are relatively new technologies and it is both scary and encouraging that the brute force bots are attempting to compromise those users. 'galbatorix' has to be my favorite name in this list, just because it is hillarious. 'binbin' is a close second. I'm also suprised to find attempts at mispelled user names such as 'icang' (probably a mispelling of icinga) and 'ubnt' (probably a mispelling of ubuntu). I'm not sure if this is because the bots have mispellings from incompetent operators, or if this is an attempt to gain access through an accidentaly created user. I can totally see how an admin setting up a service would mistype 'adduser icang' and then run 'adduser icinga' without cleaning up after themself.


Next, lets look at the attackers:


Count | Source IP
--- | ---
   7749 | 103.41.124.59
   3931 | 144.0.0.60
   2247 | 62.210.180.180
   2247 | 115.239.228.4
   1992 | 62.210.180.207
   1698 | 115.231.222.45
    406 | 62.210.142.105
    344 | 122.225.109.117
    272 | 122.225.109.219
    191 | 61.174.51.220
    189 | 61.174.51.221
    186 | 61.174.51.216
    143 | 122.225.97.79
    140 | 62.210.82.138
    115 | 62.210.140.121
    101 | 62.210.84.132
     95 | 62.210.82.85
     87 | 62.210.140.129
     22 | 62.210.82.152
     16 | 211.100.70.54
      9 | 122.225.97.71
      5 | 117.21.191.46
      1 | 87.106.73.53
      1 | 87.106.18.230
      1 | 85.95.246.191
      1 | 85.95.227.153
      1 | 70.33.231.36
      1 | 62.14.247.220
      1 | 61.135.223.212
      1 | 24.103.181.180
      1 | 222.42.146.175
      1 | 219.153.15.122
      1 | 217.160.135.104
      1 | 210.46.81.49
      1 | 202.71.103.169
      1 | 173.8.160.24


Thi s tells us more or less what we expect. We have two kinds of attacks going on. One kind of attack will brute force passwords against a single server until its database is exhausted, another kind will try a single password against its list of servers, before circling around and trying a second password. This second kind is more sophisticated because it hides from tools like fail2ban.

Conclusion: I should really turn off password auth on this server. Keys only is the way to go, still, it is impressive that 43K attempts were made in about 24 hours. It is interesting that despite this server having ipv6, no brute force attacks have been made over ipv6. This is likely due to the big-sky problem, which may be a topic for an upcomming post. It is also interesting that root accounts for almost all of the brute force attacks and regular users are very few. It is worth noting that the analysis could be performed over a longer period, data from a week or a month or more may provide more results.
